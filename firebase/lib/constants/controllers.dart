import 'package:firebase/controllers/auth_controller.dart';
import 'package:firebase/controllers/cart_controller.dart';
import 'package:firebase/controllers/chat_controller.dart';
import 'package:firebase/controllers/products_controller.dart';

AuthController authController = AuthController.instance;
ProductsController productsController = ProductsController.instance;
CartController cartController = CartController.instance;
ChatController chatController = ChatController.instance;