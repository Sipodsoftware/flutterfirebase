import 'package:firebase/models/user.dart';
import 'package:firebase/screens/auth/auth_view.dart';
import 'package:firebase/screens/home/home_view.dart';
import 'package:firebase/services/firestore_service.dart';
import 'package:firebase/services/logger.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();

  FirebaseAuth _auth = FirebaseAuth.instance;
  GoogleSignIn _googleSignin = GoogleSignIn();

  late Rx<User?> firebaseUser = Rx<User?>(_auth.currentUser);
  Rx<GoogleSignInAccount?> googleAccount = Rx<GoogleSignInAccount?>(null);

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  RxBool isLoginWidgetDisplayed = true.obs;
  late Rx<UserModel> userModel;

  @override
  void onInit() {
    super.onInit();
    userModel = UserModel().obs;
  }

  @override
  void onReady() {
    super.onReady();

    _auth.userChanges().listen((User? user) async {
      firebaseUser = Rx<User?>(user);
      await _setInitialScreen(user);
    });
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> _setInitialScreen(User? user) async {
    if (user == null) {
      Get.offAll(() => AuthView());
    } else {
      userModel.bindStream(
          FirestoreService.to.listenToUser(firebaseUser.value!.uid));
      Get.offAll(() => HomeView());
    }
  }

  void signUp() async {
    try {
      await _auth
          .createUserWithEmailAndPassword(
        email: email.text.trim(),
        password: password.text.trim(),
      )
          .then((value) {
        String _userId = value.user!.uid;
        _addUserToFirestore(_userId);
        _initializeUserModel(_userId);
      });
      _clearControllers();
    } catch (e) {
      Get.snackbar('Sign in Failed', e.toString());
    }
  }

  void signIn() async {
    try {
      await _auth
          .signInWithEmailAndPassword(
        email: email.text.trim(),
        password: password.text.trim(),
      )
          .then((value) {
        String _userId = value.user!.uid;
        _initializeUserModel(_userId);
      });
      _clearControllers();
    } catch (e) {
      Get.snackbar('Sign in Failed', e.toString());
    }
  }

  Future<void> googleSignIn() async {
    googleAccount.value = await _googleSignin.signIn();
    final googleAuth = await googleAccount.value!.authentication;
    final credentials = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await _auth.signInWithCredential(credentials).then((value) {
      String _userId = value.user!.uid;
      print(value.user!.email);
      FirestoreService.to.addGoogleUserToFirestore(_userId, googleAccount.value);
      _initializeUserModel(_userId);
    });
  }

  Future<void> signOut() async {
    googleAccount.value = await _googleSignin.signOut();
    _auth.signOut();
  }

  Future<void> updateUserData(Map<String, dynamic> data) async {
    LoggerService.to.logger.i("UPDATED");
    FirestoreService.to.updateUserData(firebaseUser.value!.uid, data);
  }

  void changeDisplayedAuthWidget() {
    isLoginWidgetDisplayed.value = !isLoginWidgetDisplayed.value;
  }

  void _addUserToFirestore(String userId) {
    FirestoreService.to.addUserToFirestore(userId,
        name: name.text.trim(),
        email: email.text.trim(),
        password: password.text.trim());
  }

  void _initializeUserModel(String userId) async {
    userModel.value = await FirestoreService.to.getUserFromFirestore(userId);
  }

  void _clearControllers() {
    name.clear();
    email.clear();
    password.clear();
  }
}
