import 'package:firebase/models/message.dart';
import 'package:firebase/models/user.dart';
import 'package:firebase/services/firestore_service.dart';
import 'package:get/get.dart';

class ChatController extends GetxController {
  static ChatController instance = Get.find();
  RxList<UserModel> users = RxList<UserModel>([]);
  RxList<Message> messages = RxList<Message>([]);

  @override
  void onReady() {
    super.onReady();
    users.bindStream(FirestoreService.to.listenAllUsers());
  }
}
