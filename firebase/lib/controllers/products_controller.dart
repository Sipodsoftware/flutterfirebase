import 'package:firebase/models/product.dart';
import 'package:firebase/services/firestore_service.dart';
import 'package:get/get.dart';

class ProductsController extends GetxController {
  static ProductsController instance = Get.find();
  RxList<ProductModel> products = RxList<ProductModel>([]);

  @override
  void onReady() {
    super.onReady();
    products.bindStream(FirestoreService.to.listenAllProducts());
  }

}
