import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/constants/controllers.dart';
import 'package:firebase/models/cart_item.dart';
import 'package:firebase/models/product.dart';
import 'package:firebase/models/user.dart';
import 'package:firebase/services/logger.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';

class CartController extends GetxController {
  static CartController instance = Get.find();
  RxDouble totalCartPrice = 0.0.obs;

  @override
  void onReady() {
    ever(authController.userModel, changeCartTotalPrice);
  }

  void addProductToCart(ProductModel product) {
    try {
      if (_isItemAlreadyAdded(product)) {
        Get.snackbar("Check your cart!", "${product.name} is already added.");
      } else {
        String itemId = Uuid().v1().toString();
        print(Uuid().options.toString());
        authController.updateUserData({
          "cart": FieldValue.arrayUnion([
            {
              "id": itemId,
              "productId": product.id,
              "name": product.name,
              "quantity": 1,
              "price": product.price,
              "image": product.image,
              "cost": product.price,
            }
          ])
        });
        Get.snackbar("Item added", "${product.name} was added to your cart");
      }
    } catch (e) {
      Get.snackbar("Error", "Cannot add item.");
      debugPrint(e.toString());
    }
  }

  void removeCartItem(CartItemModel cart) {
    try {
      authController.updateUserData({
        "cart": FieldValue.arrayRemove([cart.toJson()])
    });
    } catch (e) {
      Get.snackbar("Error", "Cannot remove this item");
      debugPrint(e.toString());
    }
  }

  changeCartTotalPrice(UserModel userModel) {
    totalCartPrice.value = 0.0;
    for (var item in userModel.cart!) {
      totalCartPrice.value += item.cost!;
    }
  }

  bool _isItemAlreadyAdded(ProductModel product) {
    return authController.userModel.value.cart!
        .where((item) => item.productId == product.id)
        .isNotEmpty;
  }

  void decreaseQuantity(CartItemModel item) {
    if (item.quantity == 1) {
      removeCartItem(item);
    } else {
      removeCartItem(item);
      if(item.quantity!=null){
        item.quantity = item.quantity! -1;
      }
      authController.updateUserData({
        "cart": FieldValue.arrayUnion([item.toJson()])
      });
    }
  }

  void increaseQuantity(CartItemModel item) {
    removeCartItem(item);
    if(item.quantity != null) item.quantity = item.quantity!+1;
    LoggerService.to.logger.i({"quantity": item.quantity});
    authController.updateUserData({
      "cart": FieldValue.arrayUnion([item.toJson()])
    });
  }
}
