import 'package:firebase/controllers/auth_controller.dart';
import 'package:firebase/controllers/cart_controller.dart';
import 'package:firebase/controllers/chat_controller.dart';
import 'package:firebase/controllers/products_controller.dart';
import 'package:get/get.dart';

class ControllersBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<AuthController>(AuthController(), permanent: true);
    Get.put<CartController>(CartController(), permanent: true);
    Get.put<ChatController>(ChatController(), permanent: true);
    Get.put<ProductsController>(ProductsController(), permanent: true);
  }
}