import 'package:flutter/material.dart';

class AppHeader {
  static home({
    required String title,
    required Future<void> onBack,
  }) {
    return PreferredSize(
      preferredSize: Size.fromHeight(40.0),
      child: AppBar(
        backgroundColor: Colors.blue,
        title: Text(title),
        actions: [
          IconButton(
              padding: EdgeInsets.zero,
              icon: Icon(
                Icons.logout,
              ),
              iconSize: 42.0,
              onPressed:() async{
                await onBack;
              }),
        ],
      ),
    );
  }

  static login({
    required String title,
  }) {
    return PreferredSize(
      preferredSize: Size.fromHeight(40.0),
      child: AppBar(
        backgroundColor: Colors.blue,
        title: Text(title),
      ),
    );
  }
}
