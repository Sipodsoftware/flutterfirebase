import 'package:firebase/constants/controllers.dart';
import 'package:firebase/screens/chat/widgets/chat_body_widget.dart';
import 'package:flutter/material.dart';

class ChatView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              ChatBodyWidget(users: chatController.users),
            ],
          ),
        ),
      ),
    );
  }
}
