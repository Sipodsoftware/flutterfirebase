
import 'package:firebase/constants/asset_paths.dart';
import 'package:firebase/constants/controllers.dart';
import 'package:firebase/screens/auth/widgets/login_view.dart';
import 'package:firebase/screens/auth/widgets/register_view.dart';
import 'package:firebase/widgets/bottom_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Obx(() => SingleChildScrollView(
          child: Stack(
            children: [
              // Image.asset(
              //   bg3,
              //   width: double.infinity,
              //   height: MediaQuery.of(context).size.height,
              //   fit: BoxFit.cover,
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.width / 1.3),
                  SizedBox(
                    height: MediaQuery.of(context).size.width / 70,
                  ),
                  Visibility(
                      visible: authController.isLoginWidgetDisplayed.value,
                      child: LoginView()),
                  Visibility(
                      visible: !authController.isLoginWidgetDisplayed.value,
                      child: RegisterView()),
                  SizedBox(
                    height: 10,
                  ),
                  Visibility(
                    visible: authController.isLoginWidgetDisplayed.value,
                    child: BottomTextWidget(
                      onTap: () {
                        authController.changeDisplayedAuthWidget();
                      },
                      text1: "Don\'t have an account?",
                      text2: "Create account!",
                    ),
                  ),
                  Visibility(
                    visible: !authController.isLoginWidgetDisplayed.value,
                    child: BottomTextWidget(
                      onTap: () {
                        authController.changeDisplayedAuthWidget();
                      },
                      text1: "Already have an account?",
                      text2: "Sign in!!",
                    ),
                  ),
                  Divider(height: 50,),
                  Visibility(
                    visible: authController.isLoginWidgetDisplayed.value,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton.extended(
                          onPressed: () async {
                            authController.googleSignIn();
                          },
                          icon: SizedBox(child: Image.asset(google), height: 40,),
                          label: Text('Sign in with Google', style: TextStyle(color: Colors.black),),
                          backgroundColor: Colors.white,
                        ),
                      ],
                    ),
                  )
                ],
              ),

              // Positioned(
              //   top: MediaQuery.of(context).size.height / 6,
              //   left: 20,
              //   child: Image.asset(
              //     logo2,
              //     width: 140,
              //   ),
              // )
            ],
          ),
        ),)
    );
  }
}
