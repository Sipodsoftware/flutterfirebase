import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/controllers/auth_controller.dart';
import 'package:firebase/models/message.dart';
import 'package:firebase/models/product.dart';
import 'package:firebase/models/user.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirestoreService {
  static FirestoreService get to => Get.find();

  FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  String usersCollectionPath = 'users';

  getCollection(String collectionPath) {
    return _firebaseFirestore.collection(collectionPath);
  }

  Stream<List<ProductModel>> listenAllProducts() {
    return _firebaseFirestore.collection('products').snapshots().map((query) =>
        query.docs.map((e) => ProductModel.fromMap(e.data())).toList());
  }

  Future<UserModel> getUserFromFirestore(String userId) async {
    return _firebaseFirestore
        .collection(usersCollectionPath)
        .doc(userId)
        .get()
        .then((doc) => UserModel.fromSnapshot(doc));
  }

  Stream<UserModel> listenToUser(String userId) {
    return _firebaseFirestore
        .collection(usersCollectionPath)
        .doc(userId)
        .snapshots()
        .map((snapshot) => UserModel.fromSnapshot(snapshot));
  }

  Stream<List<UserModel>> listenAllUsers() {
    return _firebaseFirestore
        .collection(usersCollectionPath)
        .snapshots()
        .map((query) => query.docs.map((e) => UserModel.fromSnapshot(e)).toList());
  }

  void addUserToFirestore(String userId,
      {required String name, required String email, String? password}) {
    _firebaseFirestore.collection(usersCollectionPath).doc(userId).set({
      "name": name,
      "id": userId,
      "email": email,
      "password": password,
      "cart": []
    });
  }

  void addGoogleUserToFirestore(String userId, GoogleSignInAccount? google) {
    _firebaseFirestore.collection(usersCollectionPath).doc(userId).set({
      "name": google!.displayName,
      "id": userId,
      "email": google.email,
      "google": true,
      "cart": []
    });
  }

  void updateUserData(String userId, Map<String, dynamic> data) {
    _firebaseFirestore.collection(usersCollectionPath).doc(userId).update(data);
  }


  Stream<List<Message>> getMessages(String idUser) {
    return _firebaseFirestore
        .collection('chats/$idUser/messages')
        .orderBy(MessageField.createdAt, descending: true)
        .snapshots()
        .map((event) => event.docs.map((e) => Message.fromJson(e)).toList());
  }

  Future<void> uploadMessage(String idUser, String message) async {
    var user = Get.find<AuthController>().userModel.value;
    final refMessages =
        getCollection('chats/$idUser/messages');

    final newMessage = Message(
      idUser: user.id,
      username: user.name,
      message: message,
      createdAt: DateTime.now(),
    );
    await refMessages.add(newMessage.toJson());

    final refUsers = getCollection('users');
    await refUsers.doc(idUser).update({"lastMessageTime": DateTime.now()});
  }
}
