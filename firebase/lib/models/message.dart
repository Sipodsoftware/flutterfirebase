import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/utils.dart';

class MessageField {
  static final String createdAt = 'createdAt';
}

class Message {
  final String? idUser;
  final String? username;
  final String? message;
  final DateTime? createdAt;

  const Message({
     this.idUser,
     this.username,
     this.message,
     this.createdAt,
  });

  static Message fromJson(DocumentSnapshot<Map<String, dynamic>> json) => Message(
    idUser: json['idUser'],
    username: json['username'],
    message: json['message'],
    createdAt: Utils.toDateTime(json['createdAt'])!,
  );

  Map<String, dynamic> toJson() => {
    'idUser': idUser,
    'username': username,
    'message': message,
    'createdAt': Utils.fromDateTimeToJson(createdAt!),
  };
}