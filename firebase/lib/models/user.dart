import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/models/cart_item.dart';
import 'package:firebase/utils.dart';

class UserModel {
  static const ID = "id";
  static const NAME = "name";
  static const EMAIL = "email";
  static const CART = 'cart';

  String? id;
  String? name;
  String? email;
  List<CartItemModel>? cart;
  DateTime? lastMessageTime;

  UserModel({this.id, this.name, this.email, this.cart, this.lastMessageTime});

  UserModel.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> data) {
    name = data[NAME];
    email = data[EMAIL];
    id = data[ID];
    cart = _convertCartItems(data[CART] ?? []);
  }

  static UserModel fromJson(Map<String, dynamic> json) => UserModel(
    id: json[ID],
    name: json[NAME],
    email: json[EMAIL],
    lastMessageTime: Utils.toDateTime(json['lastMessageTime']),
  );

  List<CartItemModel> _convertCartItems(List cartFromFirebase) {
    List<CartItemModel> _result = [];

    cartFromFirebase.forEach((element) {
      _result.add(CartItemModel.fromMap(element));
    });

    return _result;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'email': email,
    'lastMessageTime': lastMessageTime,
  };

  UserModel copyWith({
    String? id,
    String? name,
    String? email,
    DateTime? lastMessageTime,
  }) =>
      UserModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        lastMessageTime: lastMessageTime,
      );
}
