import 'package:firebase/services/firestore_service.dart';
import 'package:firebase/services/logger.dart';
import 'package:get/get.dart';

registerDependencies(){
  Get.put(FirestoreService(), permanent: true);
  Get.put(LoggerService(), permanent: true);
}